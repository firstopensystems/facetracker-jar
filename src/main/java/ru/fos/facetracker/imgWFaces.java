package ru.fos.facetracker;

import org.openimaj.image.processing.face.detection.DetectedFace;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * Created by lazareveugene on 27.09.17.
 */
public class imgWFaces {
    public BufferedImage img;
    public List<DetectedFace> faces;

    public imgWFaces(BufferedImage img, List<DetectedFace> faces) {
        this.img = img;
        this.faces = faces;
    }
}

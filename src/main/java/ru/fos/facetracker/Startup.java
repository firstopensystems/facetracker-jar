package ru.fos.facetracker;

import java.io.IOException;

/**
 * Created by lazareveugene on 22.09.17.
 */
public class Startup {

    private static Boolean shutdownFlag = false;

    public static void main(String[] args) throws IOException, InterruptedException {
        shutdownFlag = false;
        Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    @Override
                    public void run() {
                        System.out.println("ShutdownHook");
                        Startup.stop(args);
                    }
                }
        );
        System.out.println("Started");

        new Deamon().run();

        while (!shutdownFlag) {
            synchronized (shutdownFlag) {
                try {
                    shutdownFlag.wait(1000);
                } catch (InterruptedException ex) {

                }
            }
        }
    }

    public static void stop(String[] args) {
        System.out.println("Stop");
        shutdownFlag = true;
        synchronized (shutdownFlag) {
            shutdownFlag.notifyAll();
        }
    }
}

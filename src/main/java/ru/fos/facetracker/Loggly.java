/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.fos.facetracker;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author lazareveugene
 */
public class Loggly {

    public static boolean Exception(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client,exception," + tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "text/plain");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean Json(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client,json," + tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean Error(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client,error," + tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "text/plain");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean Dump(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client,dump," + tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "text/plain");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean Info(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client,info," +   tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "text/plain");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean Tag(String tags, String body) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("https://logs-01.loggly.com/inputs/8ca3a398-755c-4a6e-afa5-85468868c2ad/tag/client," + tags + "/").openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "text/plain");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(Transliterator.transliterate(body));
                wr.flush();
            }

            int responseCode = conn.getResponseCode();
            return responseCode == 200;
        } catch (Exception ex) {
            return false;
        }
    }
}

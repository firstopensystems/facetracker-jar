package ru.fos.facetracker;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import com.github.sarxos.webcam.ds.v4l4j.V4l4jDriver;
import com.github.sarxos.webcam.util.ImageUtils;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Deamon implements Runnable {
    private static final HaarCascadeDetector detector = new HaarCascadeDetector();
    private List<Webcam> webcams = null;

    static {
        Webcam.setDriver(new V4l4jDriver());
    }
    public Deamon() {

        webcams = Webcam.getWebcams();
        for (Webcam webcam : webcams) {
            Dimension[] sizes = webcam.getViewSizes();
            Dimension best = WebcamResolution.VGA.getSize();
            for (Dimension dim : sizes) {
                if (dim.getWidth() * dim.getHeight() > best.getWidth() * best.getHeight()) {
                    best = dim;
                }
            }
            webcam.setViewSize(best);
            webcam.open(true);
        }
    }

    @Override
    public void run() {
        boolean isPreviosHasFaces = false;
        while (true) {
            List<imgWFaces> faces = new ArrayList<>();
            for (Webcam webcam : webcams) {
                if (!webcam.isOpen()) {
                    continue;
                }
                BufferedImage img = webcam.getImage();
                faces.add(new imgWFaces(img, detector.detectFaces(ImageUtilities.createFImage(img))));
            }
            if (!isPreviosHasFaces) {
                faces.stream().filter(face -> face.faces.stream().anyMatch(f -> f.getConfidence() > 20)).forEach((img) -> {
//                    try {
//                        ImageIO.write(img.img, ImageUtils.FORMAT_PNG, new File("" + new Date().getTime() + ".png"));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    try {
                        HttpsURLConnection con = (HttpsURLConnection)
                                new URL("https://www.corezoid.com/api/1/json/public/273876/51f6146160918ef28adad8430111e95475411023").openConnection();
                        con.setRequestMethod("POST");
                        con.setDoOutput(true);
                        con.setRequestProperty("Content-Type", "application/json");
                        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                        wr.writeBytes("{\"face\":\"true\"}");
                        wr.flush();
                        wr.close();

                        int resp = con.getResponseCode();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
            isPreviosHasFaces = faces.stream().anyMatch(face -> face.faces.stream().anyMatch(f -> f.getConfidence() > 20));
        }
    }
}